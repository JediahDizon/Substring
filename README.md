# Substring
For Synopsys code reviewers. This is a basic implementation of a substring function.

## Usage
Using a Javascript compiler (ie. NodejS), run the `index.js` file.
- CMD/Terminal: `node index.js`

This will run a series of tests using the implemented substring function.

## Screenshot
![Current Screenshot](https://gitlab.com/JediahDizon/Substring/raw/master/doc/screenshots/Screenshot.png "Screenshot")
